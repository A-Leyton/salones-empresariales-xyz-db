--- Autor: Andres Leyton ---
--- FechaCreación: 22-05-2021 ---
--- Descripción: SP_INS_Clientes encargado de insetar en la tabla Salones ---

CREATE PROCEDURE SP_INS_Salones
	@pReserva VARCHAR(100) = NULL
	, @pFechaEvento DATETIME = NULL
	, @pCantidadPersonas INT = NULL
	, @pID_Motivo INT = NULL
	, @pID_EstadoConfirmadoNoConfirmado INT = NULL
	, @pFechaRegistro DATETIME = NULL
	, @pEstado BIT = NULL
AS
BEGIN
	INSERT INTO Salones 
		(Reserva
		, FechaEvento
		, ID_Motivo
		, ID_EstadoConfirmadoNoConfirmado
		, FechaRegistro
		, Estado)
	VALUES
		(@pReserva
		, @pFechaEvento
		, @pID_Motivo
		, @pID_EstadoConfirmadoNoConfirmado
		, @pFechaRegistro
		, @pEstado)
	SELECT SCOPE_IDENTITY() ID
END
