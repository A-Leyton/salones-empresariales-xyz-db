--- Autor: Andres Leyton ---
--- FechaCreación: 22-05-2021 ---
--- Descripción: SP_INS_Clientes encargado de insetar en la tabla Clientes ---

CREATE PROCEDURE SP_INS_Clientes
	@pIdentificacion VARCHAR(12) = NULL
	, @pNombreApellidos VARCHAR(48) = NULL
	, @pTelefono VARCHAR(11) = NULL
	, @pCorreo VARCHAR(40) = NULL
	, @pID_Departamentos INT = NULL
	, @pID_Ciudad INT = NULL
	, @pIEdad INT = NULL
	, @pFechaRegistro DATETIME = NULL
	, @pEstado BIT = NULL
AS
BEGIN
	INSERT INTO Clientes 
		(Identificacion
		, NombreApellidos
		, Telefono
		, Correo
		, ID_Departamentos
		, ID_Ciudad
		, FechaRegistro
		, Estado)
	VALUES
		(@pIdentificacion
		, @pNombreApellidos
		, @pTelefono
		, @pCorreo
		, @pID_Departamentos
		, @pID_Ciudad
		, @pFechaRegistro
		, @pEstado)
	SELECT SCOPE_IDENTITY() ID
END
