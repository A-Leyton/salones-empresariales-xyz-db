CREATE PROCEDURE SEL_Departamentos
AS
BEGIN
	SELECT ID_Departamentos
		, Departamento
	FROM Departamentos
	WHERE Estado = 1
END