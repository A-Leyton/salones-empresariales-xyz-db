--- Autor: Andres Leyton ---
--- FechaCreación: 22-05-2021 ---
--- Descripción: Creacion Módelo para logica ---

--//----//----//---/---////---///----///-//--//
--- Registro Usuario ---
--//----//----//---/---////---///----///-//--//
CREATE TABLE Departamentos (
 ID_Departamentos INT PRIMARY KEY IDENTITY(1,1) NOT NULL
 , Departamento VARCHAR(50) NOT NULL
 , FechaRegistro DATETIME NOT NULL
 , Estado BIT DEFAULT 0
)
CREATE TABLE Ciudad (
 ID_Ciudad INT PRIMARY KEY IDENTITY(1,1) NOT NULL
 , CIudad VARCHAR(50) NOT NULL
 , FechaRegistro DATETIME NOT NULL
 , Estado BIT DEFAULT 0
)

CREATE TABLE Clientes (
 ID INT IDENTITY (1,1) PRIMARY KEY NOT NULL
 , Identificacion VARCHAR(12) NOT NULL
 , NombreApellidos VARCHAR(48) NOT NULL
 , Telefono VARCHAR(11) NOT NULL
 , Correo VARCHAR(40) NOT NULL
 , ID_Departamentos INT FOREIGN KEY (ID_Departamentos) REFERENCES Departamentos(ID_Departamentos) NOT NULL
 , ID_Ciudad INT FOREIGN KEY (ID_Ciudad ) REFERENCES Ciudad(ID_Ciudad ) NOT NULL
 , Edad INT NOT NULL
 , FechaRegistro DATETIME NOT NULL
 , Estado BIT DEFAULT 0 NOT NULL
)
--//----//----//---/---////---///----///-//--//
--- Registro Salones ---
--//----//----//---/---////---///----///-//--//
CREATE TABLE Motivo (
  ID_Motivo INT PRIMARY KEY IDENTITY (1,1) NOT NULL
  , Motivo VARCHAR(100) NOT NULL
  , FechaRegistro DATETIME NOT NULL
  , Estado BIT DEFAULT 0 NOT NULL
)
CREATE TABLE EstadoConfirmadoNoConfirmado (
  ID_EstadoConfirmadoNoConfirmado INT PRIMARY KEY IDENTITY (1,1)  NOT NULL
  , EstadoConfirmadoNoConfirmado VARCHAR(100) NOT NULL
  , FechaRegistro DATETIME NOT NULL
  , Estado BIT DEFAULT 0 NOT NULL
)
CREATE TABLE Salones (
 ID INT IDENTITY (1,1) PRIMARY KEY NOT NULL
 , Reserva VARCHAR(100) NOT NULL
 , FechaEvento DATETIME NOT NULL
 , CantidadPersonas INT NOT NULL
 , ID_Motivo INT FOREIGN KEY REFERENCES Motivo(ID_Motivo) NOT NULL
 , ID_EstadoConfirmadoNoConfirmado INT FOREIGN KEY REFERENCES EstadoConfirmadoNoConfirmado(ID_EstadoConfirmadoNoConfirmado) NOT NULL
 , FechaRegistro DATETIME NOT NULL
 , Estado BIT DEFAULT 0 NOT NULL
)

